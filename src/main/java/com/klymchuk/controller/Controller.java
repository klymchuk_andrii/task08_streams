package com.klymchuk.controller;

import com.klymchuk.model.CounterWords;
import com.klymchuk.model.LambdaFunctions;
import com.klymchuk.model.OperationOnListOfInteger;
import com.klymchuk.model.PatternCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class Controller {
    LambdaFunctions lambdaFunctions;
    Scanner scanner;
    PatternCommand patternCommand;
    OperationOnListOfInteger operationOnListOfInteger;
    CounterWords counterWords;

    public Controller() {
        lambdaFunctions = new LambdaFunctions();
        scanner = new Scanner(System.in);
        patternCommand = new PatternCommand();
        operationOnListOfInteger = new OperationOnListOfInteger();
        counterWords = new CounterWords();
    }

    public int maximumValue(int firstNumber, int secondNumber, int thirdNumber) {
        return lambdaFunctions.maximumValue(firstNumber, secondNumber, thirdNumber);
    }

    public double averageNumber(int firstNumber, int secondNumber, int thirdNumber) {
        return lambdaFunctions.average(firstNumber, secondNumber, thirdNumber);
    }

    public String getCommand() {
        return patternCommand.getNameOfOneCommand(
                scanner.nextLine(), scanner.nextLine());
    }

    public String getListOfInteger(){
        return operationOnListOfInteger.getList().toString();
    }

    public String getStatisticOfRandomInteger() {
        return "statistic: " + operationOnListOfInteger.getStatistic();
    }

    public String getElementBiggerAverage(){
        return "element that bigger than average"
                +operationOnListOfInteger.getElementThatBiggerThanAverage().toString();
    }

    public String  getNumberOfUniqueWords(){
        long number = counterWords.getNumberOfUniqueWords(scanner.nextLine());
        return "Number Of unique words: "+ Long.toString(number);
    }

    public String getSortedListOfUniqueWords(){
        return "List of Unique words"+counterWords.sortedListOfUniqueWords().toString();
    }

    public String countWords(){
        return "Count of words: \n"+counterWords.countWords().toString();
    }

    public String countWordsWithoutUpperCaseLetters(){
        return "Count of words except upper case characters: \n"+counterWords
                .countWordsWithoutUpperCaseLetters().toString();
    }
}
