package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
    private Controller controller;
    private static Logger logger;

    public View(){
        controller = new Controller();
        logger = LogManager.getLogger(View.class);
    }

    public void show(){
        logger.trace(controller.maximumValue(2,3,5));

        logger.trace(controller.averageNumber(1,3,5));

       // logger.trace(controller.getCommand());

        logger.trace(controller.getListOfInteger());

        logger.trace(controller.getStatisticOfRandomInteger());

        logger.trace(controller.getElementBiggerAverage());

        logger.trace(controller.getNumberOfUniqueWords());

        logger.trace(controller.getSortedListOfUniqueWords());

        logger.trace(controller.countWords());

        logger.trace(controller.countWordsWithoutUpperCaseLetters());
    }
}
