package com.klymchuk.model;

@FunctionalInterface
interface MaxValue{
    int maximumValue(int firstNumber,int secondNumber,int thirdNumber);
}

@FunctionalInterface
interface Average{
    double average(int firstNumber,int secondNumber,int thirdNumber);
}

public class LambdaFunctions {
    public int maximumValue(int firstNumber,int secondNumber,int thirdNumber){
        MaxValue maxValue = (a,b,c)->(a>b?a>c?a:c:b>c?b:c);
        return maxValue.maximumValue(firstNumber,secondNumber,thirdNumber);
    }

    public double average(int firstNumber,int secondNumber,int thirdNumber){
        Average averageNumber = (a,b,c)->((a+b+c)/3);
        return averageNumber.average(firstNumber,secondNumber,thirdNumber);
    }
}
