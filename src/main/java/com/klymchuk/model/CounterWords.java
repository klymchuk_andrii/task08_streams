package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CounterWords {

    private String text;

    public long getNumberOfUniqueWords(String text) {
        Stream<String> stringStream = Stream.of(text.split(""));
        this.text = text;
        long numberOfUniqueWords = stringStream
                .filter(s -> !s.equals(" "))
                .map(s -> s.toUpperCase())
                .distinct()
                .count();
        return numberOfUniqueWords;
    }

    public List<String> sortedListOfUniqueWords() {
        List<String> list = new ArrayList<>();
        Stream<String> stringStream = Stream.of(text.split(""));

        list = stringStream
                .filter(s -> !s.equals(" "))
                .map(s -> s.toUpperCase())
                .distinct()
                .sorted()
                .map(s -> s.toLowerCase())
                .collect(Collectors.toList());

        return list;
    }

    public Map<String,Long> countWords() {
        Stream<String> stringStream = Stream.of(text.split(""));

        Map<String, Long> map = stringStream
                .filter(s -> !s.equals(" "))
                .map(s -> s.toLowerCase())
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        return map;
    }

    public Map<String,Long> countWordsWithoutUpperCaseLetters() {
        Stream<String> stringStream = Stream.of(text.split(""));

        Map<String, Long> map = stringStream
                .filter(s -> !s.equals(" "))
                .filter(s->s.equals(s.toLowerCase()))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        return map;
    }

}
