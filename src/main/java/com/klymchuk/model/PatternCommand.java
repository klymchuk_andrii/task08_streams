package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;

interface Command{
    String getName(String name);
}

class CommandImp implements Command{
    public String getName(String name){
        return name;
    }

}

public class PatternCommand {

    public String getNameOfOneCommand(String nameOfFunction,String name){
        String rezult = "command ";

        List<Command> availableCommands = new ArrayList<>();

        availableCommands.add(new Command() {
            @Override
            public String getName(String value) {
                return value;
            }
        });

        availableCommands.add(value->{return value;});

        availableCommands.add(new CommandImp()::getName);

        Command commandImp = new CommandImp();

        availableCommands.add(commandImp);

        if(nameOfFunction.equals("lambda")){
            rezult = "as lambda function"+availableCommands.get(0).getName(name);
        }else if(nameOfFunction.equals("anonymous")){
            rezult +="as anonymous class"+availableCommands.get(1).getName(name);
        }else if(nameOfFunction.equals("reference")){
            rezult +="method reference"+availableCommands.get(2).getName(name);
        }else if(nameOfFunction.equals("object")){
            rezult +="object of command class" + availableCommands.get(3).getName(name);
        }

        return rezult;
    }
}
