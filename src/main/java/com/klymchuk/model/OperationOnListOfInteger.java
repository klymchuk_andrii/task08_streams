package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class OperationOnListOfInteger {
    private List<Integer> list;
    private Random random;
    private double average;

    public OperationOnListOfInteger() {
        list = new ArrayList<>();
        random = new Random(System.currentTimeMillis());

        for (int i = 0; i < 5; i++) {
            list.add(random.nextInt(6));
        }
        average = 0;
    }

    public List<Integer> getList() {
        return list;
    }

    public String getStatistic() {
        String statistic = "";

        int max = list.stream().max(Integer::compareTo).get();

        int min = list.stream().min(Integer::compareTo).get();

        double average = list.stream()
                .mapToInt((i) -> i)
                .average().getAsDouble();
        this.average = average;

        int sum = list.stream().mapToInt((i) -> i).sum();

        int sumByReduce = list.stream().reduce((a,b)->a+b).get();


        statistic += "max: " + Integer.toString(max);
        statistic += ", min: " + Integer.toString(min);
        statistic += ", average: " + Double.toString(average);
        statistic += ",sum: " + Integer.toString(sum);
        statistic += ",sum by reduce: " + Integer.toString(sumByReduce);

        return statistic;
    }

    public List<Integer> getElementThatBiggerThanAverage(){
        List<Integer> elementsBiggerAverage = new ArrayList<>();
        elementsBiggerAverage = list.stream()
                .filter(e->e>average)
                .collect(Collectors.toList());
        return elementsBiggerAverage;
    }

}
